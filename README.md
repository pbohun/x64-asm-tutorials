# x64 Assembly Language Tutorials

This repository contains intermediate level assembly language programs.
These programs are primarily intended for fun and learning assembly language and not for production.
These programs are written in FASM for x64 Linux.

## Requirements
1. FASM : https://flatassembler.net/download.php
2. Linux on an x64 machine
2. A text editor

## Building
The assembly files are written in such a way that they only require the name of the file given to fasm to assemble the program.

Example: 
```
cd 01/
fasm main.asm
```

## License
This project is covered by the BSD three-clause license.
Please see the included LICENSE file.
