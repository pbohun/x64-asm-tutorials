; Copyright 2022 Philip Bohun
; see license file

format ELF64 executable 3

segment readable executable

entry $
	mov 	rdi, filename
	call	read_file

	; exit the program
	xor 	rdi, rdi	; exit code 0
	mov	rax, 0x3C	; sys_exit
	syscall

; read_file 
; read the contents of a file into a buffer and return the buffer
; inputs:
; 	rdi : filename
; outputs:
; 	rax : file contents buffer
read_file:
	; open file
	; filename is already in rdi
	mov 	rax, 0x02 	; open
	xor 	rsi, rsi	; O_RDONLY
	syscall

	; NOTE: we're not checking for errors

	; seek to the end of file
	mov 	rdi, rax 	; file descriptor
	xor 	rsi, rsi	; file offset
	mov	rdx, 0x02	; SEEK_END
	mov 	rax, 0x08	; lseek
	syscall

	; NOTE: the following is temporary
	; convert file size to string
	mov	rsi, rax	; file size
	mov	rdi, buf	; buffer to convert number to string
	call	uitoa

	; print the file size
	mov 	rsi, rax	; buf size
	mov 	rdi, buf	; converted string buffer
	call 	write_console

	ret

; write_console
; writes output from buffer to stdout
; inputs:
;	rdi : buf addr
;	rsi : buf size
; outputs:
;	rax : number of bytes written (-1 if error)
write_console:
	mov 	rdx, rsi	; buffer size
	mov 	rsi, rdi	; buffer addr

	; write to stdout
	mov 	rax, 0x01 	; sys_write
	mov	rdi, 0x01 	; stdout
	syscall
	ret

; read_console
; reads input from stdin into a buffer
; inputs:
;	rdi : buf addr
;	rsi : buf size
; outputs:
;	rax : number of bytes read (-1 if error)
read_console:
	mov 	rdx, rsi	; buffer size
	mov 	rsi, rdi	; buffer addr

	; read from stdin
	xor 	rax, rax 	; sys_read
	xor	rdi, rdi	; stdin
	syscall
	ret
; uitoa
; converts unsigned int to string (base 10)
; inputs:
; 	rdi : string buf
; 	rsi : number to convert
; outputs:
; 	rax : the number of digits of the string
uitoa:
	; copy our number into rax to count number of digits
	mov 	rax, rsi

	; handle the case where the number is zero
	cmp 	rax, 0x00	; compare the number to zero
	jnz 	uitoa_convert_regular
	mov 	byte [rdi], 0x30; set the first char to '0'
	inc    	esi		; move to the next char
	mov 	byte [rdi], 0x00; zero terminate the string
	mov 	rax, 0x01	; set the return num of digits
	jmp 	uitoa_end

uitoa_convert_regular:
	mov 	r10, 0x0A 	; quotient

	; count the number of digits required for conversion
	xor	rcx, rcx 	; counter for the the number of digits
uitoa_loop:
	xor 	rdx, rdx 	; make sure the remainder starts at 0
	div 	r10	 	; divide the number by 10
	inc	ecx		; increment the count by 1
	cmp 	rax, 0x00 	; compare rax to 0
	jnz 	uitoa_loop

	; inc ecx by 1 to move to zero terminator of string
	inc 	ecx

	; save the rcx value to return at the end of the func
	mov 	r8, rcx

	; set the pointer to buf to be rcx number of digits in
	add 	rdi, rcx 	; increment buffer

	; write the terminating zero to the string
	mov 	byte [rdi], 0x00
	mov 	rax, rsi 	; reset rax with the original number
	dec 	ecx 		; dec the count since we set the terminating 0

	; loop through the number and convert to string
uitoa_convert:
	xor 	rdx, rdx 	; make sure the remainder starts at 0
	dec 	rdi		; move buffer to next char
	div 	r10		; divide by 10
	add 	rdx, 0x30	; convert digit to ascii char
	mov 	byte [rdi], dl  ; move char into buf
	loopnz  uitoa_convert

	mov 	rax, r8 	; return the num of digits
uitoa_end:
	ret


segment readable writeable

filename db 'main.asm', 0
buf 	 rb 80
