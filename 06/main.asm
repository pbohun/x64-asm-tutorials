; Copyright 2022 Philip Bohun
; see license file

format ELF64 executable 3

segment readable executable

entry $
	mov 	rdi, filename
	call	read_file

	; print out the contents of the file that was read
	mov 	rsi, rdi	; file size
	mov 	rdi, rax	; buffer addr
	call	write_console

	; exit the program
	xor 	rdi, rdi	; exit code 0
	mov	rax, 0x3C	; sys_exit
	syscall

; read_file 
; read the contents of a file into a buffer and return the buffer
; inputs:
; 	rdi : filename
; outputs:
; 	rax : file contents buffer
;	rdi : the size of the file contents
; 	rsi : the size of the buffer allocation
read_file:
	; open file
	; filename is already in rdi
	mov 	rax, 0x02 	; open
	xor 	rsi, rsi	; O_RDONLY
	syscall

	; NOTE: we're not checking for errors

	mov r14, rax	 	; save the file descriptor

	; seek to the end of file
	mov 	rdi, rax 	; file descriptor
	xor 	rsi, rsi	; file offset
	mov	rdx, 0x02	; SEEK_END
	mov 	rax, 0x08	; lseek
	syscall

	; determine the number of bytes to allocate
	mov 	r15, rax 	; save the file size
	xor 	rdx, rdx	; set rdx to zero
				; rax already has number of bytes
	mov	rsi, 4096
	idiv 	rsi		; bytes / 4096 = number of pages
	inc 	rax		; pages could be 0, so increment by 1
	imul 	rax, rsi	; multiply by 4096 to get total bytes

	mov	r13, rax	; save the number of bytes to allocate

	; allocate bytes using mmap
	mov 	r10, 0x22	; MAP_PRIVATE | MAP_ANONYMOUS
	mov 	rdx, 0x03	; PROT_READ | PROT_WRITE
	mov	rsi, rax	; bytes to allocate
	xor	rdi, rdi	; addr (let the OS choose)
	mov	rax, 0x09	; mmap
	syscall

	; rax has the pointer to the allocated memory
	; NOTE: we are not checking for an error (rax == -1)

	mov	r12, rax	; save buffer address

	; rewind the file
	mov 	rdx, 0x00	; whence (SEEK_SET)
	mov	rsi, 0x00	; offset
	mov	rdi, r14	; file descriptor
	mov	rax, 0x08	; lseek
	syscall

	; read file into buffer
	mov	rdx, r13	; number of bytes to read
	mov	rsi, r12	; addr of the buffer
	mov	rdi, r14	; file descriptor
	mov 	rax, 0x00	; read 
	syscall

	; return buffer
	mov 	rax, r12	; buffer
	mov	rsi, r15	; file size
	mov	rdi, r13	; buffer size
	ret

; write_console
; writes output from buffer to stdout
; inputs:
;	rdi : buf addr
;	rsi : buf size
; outputs:
;	rax : number of bytes written (-1 if error)
write_console:
	mov 	rdx, rsi	; buffer size
	mov 	rsi, rdi	; buffer addr

	; write to stdout
	mov 	rax, 0x01 	; sys_write
	mov	rdi, 0x01 	; stdout
	syscall
	ret

; read_console
; reads input from stdin into a buffer
; inputs:
;	rdi : buf addr
;	rsi : buf size
; outputs:
;	rax : number of bytes read (-1 if error)
read_console:
	mov 	rdx, rsi	; buffer size
	mov 	rsi, rdi	; buffer addr

	; read from stdin
	xor 	rax, rax 	; sys_read
	xor	rdi, rdi	; stdin
	syscall
	ret
; uitoa
; converts unsigned int to string (base 10)
; inputs:
; 	rdi : string buf
; 	rsi : number to convert
; outputs:
; 	rax : the number of digits of the string
uitoa:
	; copy our number into rax to count number of digits
	mov 	rax, rsi

	; handle the case where the number is zero
	cmp 	rax, 0x00	; compare the number to zero
	jnz 	uitoa_convert_regular
	mov 	byte [rdi], 0x30; set the first char to '0'
	inc    	esi		; move to the next char
	mov 	byte [rdi], 0x00; zero terminate the string
	mov 	rax, 0x01	; set the return num of digits
	jmp 	uitoa_end

uitoa_convert_regular:
	mov 	r10, 0x0A 	; quotient

	; count the number of digits required for conversion
	xor	rcx, rcx 	; counter for the the number of digits
uitoa_loop:
	xor 	rdx, rdx 	; make sure the remainder starts at 0
	div 	r10	 	; divide the number by 10
	inc	ecx		; increment the count by 1
	cmp 	rax, 0x00 	; compare rax to 0
	jnz 	uitoa_loop

	; inc ecx by 1 to move to zero terminator of string
	inc 	ecx

	; save the rcx value to return at the end of the func
	mov 	r8, rcx

	; set the pointer to buf to be rcx number of digits in
	add 	rdi, rcx 	; increment buffer

	; write the terminating zero to the string
	mov 	byte [rdi], 0x00
	mov 	rax, rsi 	; reset rax with the original number
	dec 	ecx 		; dec the count since we set the terminating 0

	; loop through the number and convert to string
uitoa_convert:
	xor 	rdx, rdx 	; make sure the remainder starts at 0
	dec 	rdi		; move buffer to next char
	div 	r10		; divide by 10
	add 	rdx, 0x30	; convert digit to ascii char
	mov 	byte [rdi], dl  ; move char into buf
	loopnz  uitoa_convert

	mov 	rax, r8 	; return the num of digits
uitoa_end:
	ret


segment readable writeable

filename db 'main.asm', 0
buf 	 rb 80
